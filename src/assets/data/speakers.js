export default [
	{
		id: 'douangtavanh-kongphaly',
		name: 'Douangtavanh Kongphaly',
		position: 'Co-Oganizer GDG Vientiane & Consultant',
		company: 'NIPN-CDR',
		country: 'Laos',
		bio: `ກ່າວເປີດງານ / Welcome remarks`,
		session: 'ເປີດງານ / Welcome Remarks' // Session title for now
	},
	{
		id: 'vanvixay-thammavong',
		name: 'Vanvixay Thammavong',
		position: 'Software developer',
		company: 'LaoITdev',
		country: 'Laos',
		bio: `ໃນຫົວຂໍ້ນີ້ຈະມາເວົ້າເຖິງ ຈຸດທີ່ດີທີ່ສຸດ ຂອງ 2 ສິ່ງນີ້, ການ ບຳລຸງຮັກສາ ແລະ  ສ້າງເວັບຂອງເຮົາໃຫ້ເປັນ responsive  ໂດຍຂຽນ CSS ໜ່ອຍດຽວ / This session will talk about best parts of both libraries to build beautifull, maintainable, responsive angular application, with very little CSS`,
		session:
			'ຈະໃຊ້ Angular Material ຫຼື Bootstrap ? ຢຸດຖາມໄດ້ແລ້ວ, ເຮົາມີຄໍາຕອບໃຫ້! / Angular Material or Bootstrap? Stop asking the question, We have the answer!' // Session title for now
	},
	{
		id: 'souphaphone-chansy',
		name: 'Souphaphone Chansy',
		position: 'Executive Assistant of Country General Manager / Campaigner, Volunteer',
		company: 'LaoDerm Group',
		country: 'Laos',
		bio: `We live in a world today where it’s becoming more common to speak negatively about someone, rather than positively from reality to the cyber society. The emotional violation is existing through the digital platform occurs by  posting, or sharing negative, harmful, false, or mean content about someone else. It can include sharing personal or private information about someone else causing embarrassment or humiliation. Some cyberbullying crosses the line into unlawful or criminal behavior in society called “Cyber Bullying”. This public record can be thought of as an online reputation, which may be accessible to schools, employers, colleges, clubs, and others who may be researching an individual now or in the future. Cyberbullying can harm the online reputations of everyone involved – not just the person being bullied, but those doing the bullying or participating in it. Let’s refuse to commit to any kinds of bullying. `,
		session: 'ຮູ້ຈັກກັບໄພງຽບຂອງໂລກໄຊເບີ / The Invisible Cyber Crimes' // Session title for now
	},
	{
		id: 'jalana-vongxay',
		name: 'Jalana Vongxay',
		position: 'Back-end Engineer',
		company: 'Lao IT Dev',
		country: 'Laos',
		bio: `ທ່ານມັກຄວາມໄວບໍ່ ? ຖ້າມັກ JSON ອາດຈະໄວບໍ່ພໍສຳລັບທ່ານ ໃນ session ນີ້ສະປີກເກີຈະມາແນະນຳໃຫ້ທ່ານໄດ້ຮູ້ຈັກກັບ Protobuf ເຊິ່ງເປັນໃນການ serialize/deserialize data ແບບໃໝ່ທີ່ມາພ້ອມກັບຄວາມໄວ ແລະ ການສົ່ງຂໍ້ມູນໃນຮູບແບບ binary (demo chat app)`,
		session: 'ເມື່ອຍແລ້ວກັບ JSON ສະບາຍດີ Protobuf / Goodbye JSON, Hello Protobuf' // Session title for now
	},
	{
		id: 'sengxay-xayachack',
		name: 'Sengxay Xayachack',
		position: 'Co-Founder and CEO',
		company: 'CQ',
		country: 'Laos',
		bio: `Business Logic ຖືວ່າເປັນສິ່ງສຳຄັນສຸດຢ່າງໜຶ່ງໃນການພັດທະນາ application ເພາະຈະເປັນໂຕຕັດສິນ flow ການເຮັດວຽກຕ່າງໆຂອງລະບົບເຮົາວ່າຈະຖືກ ຫຼື ຜິດ, ໃນການບັນລະຍາຍຄັ້ງນີ້ speaker ຈະມາແນະນຳເຖິງການອອກແບບ ແລະການພັດທະນາ Business Logic ທີ່ປອດໄພ ແລະ Case study ວ່າ Hacker ສາມາດ Hack ລະບົບເຮົາຈາກການອອກແບບ Business Logic ທີ່ບໍ່ດີໄດ້ແນວໃດ / Designing a good and secure Business Logic is one of the most important processes in application development. The Business Logic will determine whether your application flow will work correctly or not. In this session, the speaker will introduce you to how to design and develop a good Business logic. Furthermore, the speaker will show you a case study on how the hacker hacks a system and takes advantage of a bad Business Logic.`,
		session: 'Business Logic ຜິດຊີວິດປ່ຽນ / The nightmares of a wrong business logic' // Session title for now
	},
	{
		id: 'valita-khamdaranikone',
		name: 'Valita Khamdaranikone',
		position: 'Marketing Manager, Graphic Design',
		company: 'GOTEDDY',
		country: 'Laos',
		bio: `-`,
		session: 'ແມ່ຍິງ ກັບໂລກຂອງເຕັກໂນໂລຊີ / Women vs Technology' // Session title for now
	},
	{
		id: 'dalasak-yodchalern',
		name: 'Dalasak Yodchalern',
		position: 'Front-End & UX/UI Design',
		company: 'Laos Startup',
		country: 'Laos',
		bio: `ຈະເວົ້າເຖິງຄວາມສຳຄັນຂອງ UX/UI , ການສ້າງ ແລະ ອອກແບບ Front-End ດ້ວຍ Flutter.`,
		session: 'ແມ່ຍິງ ກັບໂລກຂອງເຕັກໂນໂລຊີ / Women vs Technology' // Session title for now
	},
	{
		id: 'thidavone-mankhongsinh',
		name: 'Thidavone Mankhongsinh',
		position: 'WTM Ambassador Vientiane / STEM Teacher Assistant / GDG Vientiane member',
		company: 'NUOL',
		country: 'Laos',
		bio: `A talking panel with Women from Tech area with different careers`,
		session: 'ແມ່ຍິງ ກັບໂລກຂອງເຕັກໂນໂລຊີ / Women vs Technology' // Session title for now
	},
	{
		id: 'warat-wongmaneekit',
		name: 'Warat Wongmaneekit',
		position: 'Product Owner & Google Developer Expert',
		company: 'WISESIGHT',
		country: 'Thailand',
		bio: `I will teach everyone to create own assistant with is 15 mins`,
		session: 'ວິທີສ້າງຜູ່ຊ່ອຍສ່ວນໂຕພາຍໃນ 15 ນາທີ / How to create assistant with in 15 mins' // Session title for now
	},
	{
		id: 'souksakhone-sengxaya',
		name: 'Souksakhone Sengxaya',
		position: 'Co-Founder and CTO',
		company: 'CQ',
		country: 'Laos',
		bio: `ແນະນໍາກ່ຽວກັບ flutter, ການຈັດການ state ໃນ flutter, ແລະ layout ທົ່ວໄປຂອງ flutter / Introducing to flutter, flutter state management, flutter layout constraint`,
		session: 'ການພັດທະນາແອັບດ້ວຍ Flutter ແບບງ່າຍໆ / Easy Mobile App Dev with Flutter' // Session title for now
	},
	{
		id: 'bounma-inthapattha',
		name: 'Bounma Inthapattha',
		position: 'Website Development student, Graphic design freelancer',
		company: 'NUOL',
		country: 'Laos',
		bio: `ແນະນຳ workflow ຂອງການອອກແບບ, Tips ໃນການອອກແບບ. ທັງ Apps, Website ແລະ Artwork`,
		session: 'ປະຍຸກໃຊ້ UX/UI ແນວໃດໃຫ້ເຂົ້າກັບໜ້າວຽກຂອງທ່ານ / Guide you to do UX/UI' // Session title for now
	},
	{
		id: 'chittaphone-phiasakha',
		name: 'Chittaphone Phiasakha',
		position: 'Co-Founder and CTO',
		company: 'LaoIO',
		country: 'Laos',
		bio: `ສະຫຼຸບລວມ technology ຂອງ  IoT  ທີ່ ນັກພັດທະນາ ແລະ maker ໃຊ້ກັນຢູ່ໃນປັດຈຸບັນ ຕົວຢ່າງ: cloud IoT , AIOT , Arduino, Esp, MQTT, nodered ... etc`,
		session: 'ເຕັກໂນໂລຢີ IoT ທີ່ນັກພັດທະນາ ແລະຊາວນັກປະດິດຄວນຮູ້ / IoT Technology that developer & maker Should know' // Session title for now
	},
	{
		id: 'outhai-saioudom',
		name: 'Outhai Saioudom',
		position: 'Co-Founder and CTO',
		company: 'Lao IT Dev',
		country: 'Laos',
		bio: `ການເຂົ້າຮ່ວມກິດຈະກຳກັບຊຸມຊົນພາເຮົາມາໄກປານນີ້ພິຫວາ?`,
		session: 'ການເຂົ້າຮ່ວມກິດຈະກຳກັບຊຸມຊົນພາເຮົາມາໄກປານນີ້ພິຫວາ? / The Amazing of Community' // Session title for now
	},
	{
		id: 'khouanfa-siriphone',
		name: 'Khouanfa Siriphone',
		position: 'Creative Director & CO-founder',
		company: 'STELLA',
		country: 'Laos',
		bio: `ຈະເລົ່າເລື່ອງການລິເລີ່ມຂອງ STELLA ແລະ ກຸ່ມການເດີນທາງຂອງ interns ຂອງເຮົາ`,
		session: 'ການເຂົ້າຮ່ວມກິດຈະກຳກັບຊຸມຊົນພາເຮົາມາໄກປານນີ້ພິຫວາ? / The Amazing of Community' // Session title for now
	},
	{
		id: 'keomanophone-thammavong',
		name: 'Keomanophone Thammavong',
		position: 'Community Manager GDG Vientiane & COO',
		company: 'Lao IT Dev',
		country: 'Laos',
		bio: `ການເຂົ້າຮ່ວມກິດຈະກຳກັບຊຸມຊົນພາເຮົາມາໄກປານນີ້ພິຫວາ?`,
		session: 'ການເຂົ້າຮ່ວມກິດຈະກຳກັບຊຸມຊົນພາເຮົາມາໄກປານນີ້ພິຫວາ? / The Amazing of Community' // Session title for now
	},
	{
		id: 'virasack-viravong',
		name: 'Virasack Viravong',
		position: 'Co-Founder and CMO',
		company: 'Lao IT Dev',
		country: 'Laos',
		bio: `-`,
		session: 'ພິທີກອນ / MC' // Session title for now
	},
	{
		id: 'somsamay-salithongnith',
		name: 'Somsamay Salithongnith',
		position: 'Creative Graphic Designer',
		company: 'Pankham Jampa',
		country: 'Laos',
		bio: `-`,
		session: 'ພິທີກອນ / MC' // Session title for now
	}
];
