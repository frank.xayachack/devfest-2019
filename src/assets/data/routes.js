import Index from '../../views/Index.vue';
import Home from '../../views/Home.vue';
import Speakers from '../../views/Speakers.vue';
import Schedule from '../../views/Schedule.vue';

export var indexNavigationRoutes = [
  {
    name: 'ໜ້າຫຼັກ',
    path: '/',
    component: Home
  },
  {
    name: 'ຜູ້ບັນຍາຍ',
    path: '/speakers',
    component: Speakers
  },
  {
    name: 'ຕາຕະລາງງານ',
    path: '/schedule',
    component: Schedule
  }
];

export default [
  {
    name: 'Index',
    path: '/',
    component: Index,
    redirect: { name: 'Home' },
    children: [...indexNavigationRoutes]
  }
];
